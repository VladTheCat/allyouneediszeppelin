local game = {}

local Player = require "player"
local Camera = require "hump.camera"

local background = love.graphics.newImage("back.png")

function game:enter()
	love.graphics.setBackgroundColor(lume.color("#60A7F7", 255))
	world = collider.new(300)
	gravity = vector(0, 1000)
	actors = {}
	actorsRem = {}
	gt = timer.new()
	player = Player(100, 100)
	camera = Camera(player.pos.x, player.pos.y)
	time = 0
end

function game:update(dt)
	gt.update(dt)
	time = time + dt
	signal.emit("gameUpdate", dt)
	for i, v in ipairs(actors) do
		v:update(dt)
	end
	for i, v in ipairs(actorsRem) do
		for ii, vv in ipairs(actors) do
			if v == vv then
				table.remove(actors, ii)
				break
			end
		end
	end
	if keysPressed["`"] then
		state.push(console)
	end
	actorsRem = {}
	world:update(dt)
	camera:lookAt(player.pos.x, player.pos.y)
end

function game:draw()
	local w, h = background:getWidth(), background:getHeight()
	local px, py = math.floor(player.pos.x), math.floor(player.pos.y)
	for x = -1, 0 do
		for y = -1, 5 do
			local dx = w / 2 * x - px % w / 2
			local dy = h * y - py % h
			love.graphics.draw(background, dx, dy, 0, 100, 1)
		end
	end
	--[[local dx = w - player.pos.x % w
	local dy = h - player.pos.y % h
	love.graphics.print(dx.."\n"..dy, 0, 0)]]

	camera:attach()
	for i, v in ipairs(actors) do
		v:draw()
	end
	camera:detach()
end

return game
