local c = {}
c.log = {}
c.logOffset = 0
c.logXOffset = 0
c.logMaxLen = 0
c.logDrag = false
c.logDragX = 0
c.logDragY = 0
c.commandLine = ""
c.cursor = 0
c.cursorTimer = 0
c.commandLog = {}
c.commandLogN = 0
c.window = {x = 0, y = 30, w = 400, h = 300, drag = false, head = 16}
c.active = true
c.textFieldActive = true
c.drag = false
c.dragX, c.dragY = 0, 0
c.font = love.graphics.newFont("profont.ttf", 11)
--c.font = love.graphics.newFont(10)
c.font:setFilter("nearest", "nearest", 0)
c.allowUpdate = false

local keysPressed = {}
local keysReleased = {}
local buttonsPressed = {}
local buttonsReleased = {}

local function freeKeys()
	keysPressed = {}
	keysReleased = {}
	buttonsPressed = {}
	buttonsReleased = {}
end

function c:start()
end

function c.exec(str)
	local code, err = loadstring(str)
	table.insert(c.log, ">>"..str)
	table.insert(c.commandLog, str)
	local ok, ret = pcall(code)
	if ret then
		table.insert(c.log, "return: "..tostring(ret))
	end
	local len = c.font:getWidth(str)
	local len2 = c.font:getWidth(ret or "")
	c.logMaxLen = math.max(len, len2, c.logMaxLen)
	c.commandLine = ""
	c.cursor = 0
	c.logOffset = math.max(#c.log - 27, 0)
	c.commandLogN = #c.commandLog
end

function c:enter(from)
	c.active = true
	c.from = from
	freeKeys()
end

function c.step(dt)
	c.from:update(dt)
end

function c:update(dt)
	c.cursorTimer = (c.cursorTimer + dt) % 1

	local win = c.window
	if mouse.x > win.x and mouse.y > win.y
	and mouse.x < win.x + win.w and mouse.y < win.y + win.head then
		if buttonsPressed["l"] then
			c.drag = true
			c.dragX, c.dragY = mouse.x - win.x, mouse.y - win.y
			buttonsPressed["l"] = false
		end
	end
	if mouse.x > win.x and mouse.y > win.y + win.head
	  and mouse.x < win.x + win.w and mouse.y < win.y + win.w then
		if buttonsPressed["l"] then
			c.logDrag = true
			c.logDragX = mouse.x
			c.currentLogOffset = c.logXOffset
		end
	end
	if buttonsReleased["l"] then
		c.logDrag = false
		c.drag = false
	end
	if c.drag then
		win.x, win.y = mouse.x - c.dragX, mouse.y - c.dragY
	end
	if c.logDrag then
		c.logXOffset = c.currentLogOffset + (mouse.x - c.logDragX)
		c.logXOffset = lume.clamp(c.logXOffset, -c.logMaxLen, 0)
	end
	if c.textFieldActive then
		local function erase()
			if c.cursor >= 1 then
				local a = string.sub(c.commandLine, 1, c.cursor - 1)
				local b = string.sub(c.commandLine, c.cursor + 1, #c.commandLine)
				c.commandLine = a .. b
				c.cursor = c.cursor - 1
			end
		end
		if keysPressed["left"] and c.cursor > 0 then
			c.cursor = c.cursor - 1	
		elseif keysPressed["right"] and c.cursor < #c.commandLine then
			c.cursor = c.cursor + 1
		end
		if keysPressed["home"] then c.cursor = 0 end
		if keysPressed["end"] then c.cursor = #c.commandLine end
		if keysPressed["up"] then
			if c.commandLogN > 1 then
				c.commandLogN = c.commandLogN - 1
				c.commandLine = c.commandLog[c.commandLogN]
				c.cursor = #c.commandLine
			end
		end
		if keysPressed["down"] then
			if c.commandLogN < #c.commandLog then
				c.commandLogN = c.commandLogN + 1
				c.commandLine = c.commandLog[c.commandLogN]
				c.cursor = #c.commandLine
			end
		end
		if keysPressed["return"] and c.commandLine ~= "" then
			c.exec(c.commandLine)
			keysPressed["return"] = false
		end
		if keysPressed["backspace"] then
			erase()
			keysPressed["backspace"] = false
		end
		if love.keyboard.isDown("lshift") then
			if love.keyboard.isDown("backspace") then
				erase()
			elseif love.keyboard.isDown("left") and c.cursor > 0 then
				c.cursor = c.cursor - 1
			elseif love.keyboard.isDown("right") and c.cursor < #c.commandLine then
				c.cursor = c.cursor + 1
			end
		end
	end
	if buttonsPressed["wu"] then
		c.logOffset = math.max(c.logOffset - 1, 0)
	elseif buttonsPressed["wd"] then
		c.logOffset = math.min(c.logOffset + 1, #c.log - 1)
	end
	if keysPressed["`"] then
		state.pop()
		keysPressed["`"] = false
	end
	if c.allowUpdate then
		c.from:update(dt)
	end
	freeKeys()
end

function c:keypressed(k)
	keysPressed[k] = true
end

function c:keyreleased(k)
	keysReleased[k] = true
end

function c:mousepressed(x, y, b)
	buttonsPressed[b] = true
end

function c:mousereleased(x, y, b)
	buttonsReleased[b] = true
end

function c:textinput(t)
	if t ~= "`" then
		if c.textFieldActive then
			local a = string.sub(c.commandLine, 1, c.cursor)
			local b = string.sub(c.commandLine, c.cursor + 1, #c.commandLine)
			c.commandLine = a .. t .. b
			c.cursor = c.cursor + 1
		end
	end
end

function print(s)
	table.insert(c.log, tostring(s))
	io.write(tostring(s).."\n")
end

function inRect()
	local win = c.window
	love.graphics.setColor(lume.rgba(0xffffffff))
	love.graphics.rectangle("fill", win.x, win.y + win.head, win.w, win.h - win.head)
end

function c:draw()
	c.from:draw()
	local win = c.window
	local x = win.x + 3
	local y = win.y + win.head
	love.graphics.setColor(100, 100, 100, 100)
	love.graphics.rectangle("fill", win.x, win.y, win.w, win.h)
	love.graphics.setColor(255, 255, 255, 200)
	love.graphics.rectangle("line", win.x, win.y, win.w, win.h)
	love.graphics.rectangle("line", win.x, win.y, win.w, win.head)
	love.graphics.setFont(c.font)
	love.graphics.setStencil(inRect)

	
	local a = string.sub(c.commandLine, 1, c.cursor)
	local conXOff = 0--math.min(0, -c.font:getWidth(a))
	love.graphics.print(c.commandLine, x + conXOff, y)
	local w = c.font:getWidth(a)
	love.graphics.print("|", x + w - c.font:getWidth("#") / 2 + 1, y)
	for i = 1, 27 do
		local v = c.log[i + c.logOffset]
		if v then
			love.graphics.print(v, x + c.logXOffset, y + i * 10)
		end
	end
	love.graphics.setStencil()
end

return c
