--bump = require "bump"
collider = require "hardoncollider"
lume = require "lume"
state = require "hump.gamestate"
vector = require "hump.vector"
timer = require "hump.timer"
class = require "hump.class"
signal = require "hump.signal"
console = require "console"
anim8 = require "anim8"
actor = require "actor"

love.graphics.setDefaultFilter("nearest", "nearest")

local game = require "game"

function freeKeys()
	keysPressed = {}
	keysReleased = {}
	buttonsPressed = {}
	buttonsReleased = {}
end
	
function love.load()
	freeKeys()
	mouse = {}
	time = 0
	state.registerEvents()
	state.switch(game)
	love.graphics.setLineStyle("rough")
	love.graphics.setLineWidth(2)
end

function love.update(dt)
	timer.update(dt)
	time = time + dt
	mouse.x, mouse.y = love.mouse.getPosition()
end

function love.keypressed(k)
	keysPressed[k] = true
	signal.emit("press_"..k)
end

function love.keyreleased(k)
	keysReleased[k] = true
	signal.emit("release_"..k)
end

function love.mousepressed(x, y, b)
	buttonsPressed[b] = true
	signal.emit("mpress_"..b, x, y)
end

function love.mousereleased(x, y, b)
	buttonsReleased[b] = true
	signal.emit("mrelease_"..b, x, y)
end

function love.draw()
	freeKeys()
end

function setColor(col)
	love.graphics.setColor(lume.rgba(col))
end
