local actor = class{}

function actor:init(x, y, w, h)
	self.pos = vector(x or 0, y or 0)
	self.width = w or 1
	self.height = h or 1
	self.blend = 0xffffffff
	table.insert(actors, self)
end

function actor:update(dt)
end

function actor:draw()
	local x, y, w, h = self.pos.x, self.pos.y, self.width, self.height
	love.graphics.setColor(lume.rgba(self.blend))
	love.graphics.rectangle("line", x, y, w, h)
end

function actor:remove()
	table.insert(actorsRem, self)
end

return actor
