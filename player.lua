local Player = class{}
local shapes = require "hardoncollider.shapes"
--[[Player.shape = shapes.newPolygonShape({
	
})]]
local img = love.graphics.newImage("PlayerShip.png")
local fire = love.graphics.newImage("PlayerFire.png")
local grid = anim8.newGrid(14, 7, 28, 7)
local fireAnim = anim8.newAnimation(grid("1-2", 1), 0.1)
Player:include(actor)

function Player:init(x, y)
	actor.init(self, x, y, 1, 1)
	self.move = vector(0, 0)
	self.dir = vector(1, 0)
	self.altiude = 0
	self.maxAlt = 4000
	self.maxSpeed = 800
	self.highAlt = false
	self.canFly = true
	self.fire = {bottom = false, back = false}
end

function Player:update(dt)
	self.fire.bottom = false
	self.fire.back = false
	self.move = self.move + gravity * dt

	if love.keyboard.isDown("z") then
		self.fire.bottom = true
	end
	if love.keyboard.isDown("right") then
		--self.move.x = self.move.x + 700 * dt
		self.dir.x = 1
		self.fire.back = true
	elseif love.keyboard.isDown("left") then
		--self.move.x = self.move.x - 700 * dt
		self.dir.x = -1
		self.fire.back = true
	end

	self.highAlt = false
	if self.altiude > self.maxAlt - 500 then
		self.highAlt = true
	end

	if self.altiude > self.maxAlt and self.canFly then
		self.canFly = false
		gt.add(4, function() self.canFly = true end)
	end

	if not self.canFly then
		self.fire.back, self.fire.bottom = false, false
	end
	
	if not self.fire.back then
		self.move.x = self.move.x - (self.move.x / 2) * dt
	else
		self.move.x = self.move.x + 700 * dt * self.dir.x
		if not self.fire.bottom then
			if lume.sign(self.move.x) == lume.sign(self.dir.x) then
				self.move.y = self.move.y - self.move.y * 3 * dt
			end
		end
	end
	if self.fire.bottom then
		self.move.y = self.move.y - 2100 * dt
	end
	self.move.x = lume.clamp(self.move.x, -self.maxSpeed, self.maxSpeed)
	self.move.y = self.move.y - (math.abs(self.move.x / self.maxSpeed * gravity.y)) * dt
	self.move.y = lume.clamp(self.move.y, -500, 1000)
	fireAnim:update(dt)
	self.pos = self.pos + self.move * dt

	self.altiude = -self.pos.y

	--self.pos.x = self.pos.x % 800
	--self.pos.y = self.pos.y % 600
end

local testFont = love.graphics.newFont(20)

function Player:draw()
	setColor(0xffffffff)
	love.graphics.setFont(testFont)
	local x, y = math.floor(self.pos.x), math.floor(self.pos.y)
	love.graphics.draw(img, x, y, 0, self.dir.x * 2, 2, 18, 11)
	if self.fire.back then
		fireAnim:draw(fire, x, y, 0, self.dir.x * 2, 2, 31, 2)
	end
	if self.fire.bottom then
		fireAnim:draw(fire, x, y, -math.pi / 2, 2, self.dir.x * 2, 25, 3)
	end
	setColor(0xff000000)
	love.graphics.print(string.format("ALT: %d", self.altiude), x, y - 40)
	setColor(0xffff0000)
	
	local t = ""
	if self.highAlt then
		t = "WARNING! HIGH ALTITUDE!"
	end
	if not self.canFly then
		t = "ENGINE OVERHEATED"
	end
	local draw = ((time * 2 % 1 < 0.5) or not self.canFly)
	if draw then
		local w = testFont:getWidth(t) / 2
		love.graphics.print(t, x - w, y + 100)
	end
	setColor(0xffffffff)
end

return Player
